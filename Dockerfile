FROM python:3.9.18-slim-bookworm

ENV DJANGO_DB_HOST host
ENV DJANGO_DB_NAME app
ENV DJANGO_DB_USER worker
ENV DJANGO_DB_PASS worker
ENV DJANGO_DB_PORT "5432"
ENV DJANGO_DEBUG "False"
WORKDIR /app
COPY requirements.txt .
RUN apt update && \
    apt install gcc -y && \
    pip3 install -r requirements.txt --no-cache-dir && \
    pip3 install uwsgi==2.0.23 --no-cache-dir && \
    apt remove gcc -y && \
    rm -rf /var/cache/apt/archives/*

COPY . .

RUN chmod +x entrypoint.sh
EXPOSE 8000
ENTRYPOINT [ "/app/entrypoint.sh" ]